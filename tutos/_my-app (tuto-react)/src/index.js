import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


/*class Square extends React.Component {
  render() {
    return (
      <button
        className="square"
        onClick={() => this.props.onClick()}
      >
        {this.props.value}
      </button>
    );
  }
}*/

// Composant pur Square
function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}



// Board
class Board extends React.Component {

  // Rendu du composant pur Square
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)} // onClick = event de Square = this.props.onClick()
      />
    );
  }

  // Rendu du Board
  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}



class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        { squares: Array(9).fill(null), }
      ],
      stepNumber: 0,
      xIsNext: true,
    };
  }

  // Quand on click sur une case
  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice(); // Version la plus récente de de Board
    if (calculateWinner(squares) || squares[i]) { // Si case déjà remplie ou vaincqueur
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  //
  jumTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }
  
  // Rendu du Game
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ? 'Revenir au tour n°' + move : 'Revenir au début de la partie';
      return (
        <li key={move} >
          <button onClick={() => this.jumTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = winner + ' a gagné !';
    } else {
      status = 'Prochain joueur : ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => this.handleClick(i)} // handleClick() = méthode qui gère l'évenement onClick
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
  


// Winner ?
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2], // Horizontale 1
    [3, 4, 5], // Horizontale 2
    [6, 7, 8], // Horizontale 3
    [0, 3, 6], // Verticale 1
    [1, 4, 7], // Verticale 2
    [2, 5, 8], // Verticale 3
    [0, 4, 6], // Diagonale 1
    [2, 4, 6], // Diagonale 2
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}